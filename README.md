git hook
Tạo một file commit-msg trong thư mục .git/hooks với nội dung
#!/bin/sh
message=$(cat $1)
pattern="^(feat|fix|docs|style|refactor|test|chore)([^)]+): .+$"
