FROM node:19-alpine
WORKDIR /app
COPY . .
RUN npm install -f
ENV TZ=Asia/Ho_Chi_Minh
ENTRYPOINT node index.js
EXPOSE 3000