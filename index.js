const express = require('express');
const app = express();
require('dotenv').config();
const { S3Client, S3, GetObjectCommand } = require("@aws-sdk/client-s3");
const mime = require('mime-types');


const accessKeyId = process.env.AWS_ACCESS_KEY_ID;
const secretAccessKey = process.env.AWS_SECRET_ACCESS_KEY;
const region = process.env.S3_REGION;
const endpoint = process.env.END_POINT;
const Bucket = process.env.S3_BUCKET;
const QRCode = require('qrcode')



app.set('json spaces', 5); // to pretify json response

const PORT = process.env.PORT;
const fileparser = require('./fileparser');

// app.get('/', (req, res) => {
//   res.send(`
//     <h2>File Upload With <code>"Node.js"</code></h2>
//     <form action="/upload" enctype="multipart/form-data" method="post">
//       <div>Select a file:
//         <input name="file" type="file" />
//       </div>
//       <input type="submit" value="Upload" />
//     </form>

//   `);
// });
app.get('/health', async (req, res) => {
  res.status(200).json({
    message: "ok"
  });
});
app.post('/upload', async (req, res) => {
  await fileparser(req)
    .then(data => {
      res.status(200).json({
        message: "Success",
        data
      });
    })
    .catch(error => {
      res.status(400).json({
        message: "An error occurred.",
        error
      });
    });
});
app.get('/download/qrcode', async (req, res) => {
  const url = req.query.url || ROOT_DOMAIN || 'https://customer-page.hanzomaster.me';
  const tableId = req.query.tableId;
  const restaurantId = req.query.restaurantId;

  const text = `${url}/?tableId=${tableId}&restaurantId=${restaurantId}`

  QRCode.toFileStream(res, text, {
    type: 'png',
    errorCorrectionLevel: 'H',
    // version: 10,
  }, (err) => {
    if (err) {
      res.status(500).send('Error generating QR code');
      return;
    }
    res.end();
  });
});


app.get('/download/:filename', async (req, res) => {
  const filename = req.params.filename;

  const s3Client = new S3Client({
    credentials: {
      accessKeyId,
      secretAccessKey
    },
    region,
    endpoint,
  });

  const params = {
    Bucket,
    Key: filename // Tên tệp trên S3 dựa trên tham số truyền vào
  };

  const getObjectCommand = new GetObjectCommand(params);
  try {
    const getObjectResponse = await s3Client.send(getObjectCommand);
    const fileStream = getObjectResponse.Body;

    // Đặt header cho tệp tải về
    // res.setHeader("Content-Disposition", `attachment; filename=${filename}`);

    const contentType = mime.lookup(filename) || 'application/octet-stream';
    console.log(contentType);
    res.setHeader('Content-Type', contentType);

    fileStream.on("data", (chunk) => {
      res.write(chunk);
    });

    fileStream.on("end", () => {
      res.end();
    });
  } catch (error) {
    console.error("Error downloading file from S3:", error);
    res.status(500).send("Error downloading file from S3");
  }

});


app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}.`);
});
